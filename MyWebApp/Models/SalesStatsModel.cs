using System.ComponentModel;

namespace MyWebApp.Models
{
    public class SalesStatsModel
    {
        [DisplayName("Highest Sale Amount")]
        public double  HighestSale { get; set; }
        
        [DisplayName("Average Sale Amount")]
        public double AverageSaleAmount { get; set; }
        
        [DisplayName("Leading Sales Product")]
        public string LeadingProduct { get; set; }
        
        [DisplayName("Leading Sales Employee")]
        public string LeadingSalesEmployee { get; set; }
    }
}